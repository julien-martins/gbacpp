#include "SFML/Window.hpp"
#include <iostream>

int main()
{
    sf::Window _window(sf::VideoMode(800, 600), "My window");

    while(_window.isOpen())
    {
        sf::Event event;
        while( _window.pollEvent(event))
        {
            if(event.type == sf::Event::Closed) _window.close();
        }
    }
    
    return 0;
}
